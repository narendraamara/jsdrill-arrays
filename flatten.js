function flatten(nestedArray) {
    var flattenArray=[];
    for (let ele=0;ele<nestedArray.length;ele+=1) {
        if (Array.isArray(nestedArray[ele])) {
            flattenArray = flattenArray.concat(flatten(nestedArray[ele]));
        } else {
            flattenArray.push(nestedArray[ele]);
        }
    }return flattenArray;
}module.exports=flatten